﻿namespace EasyNotes
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelBrand = new System.Windows.Forms.Panel();
            this.panelRegister = new System.Windows.Forms.GroupBox();
            this.btnRegister = new System.Windows.Forms.Button();
            this.lblRegisterRePassword = new System.Windows.Forms.Label();
            this.lblRegisterPassword = new System.Windows.Forms.Label();
            this.lblRegisterFullname = new System.Windows.Forms.Label();
            this.lblRegisterName = new System.Windows.Forms.Label();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.panelLogin = new System.Windows.Forms.GroupBox();
            this.btnLogin = new System.Windows.Forms.Button();
            this.lblLoginPassword = new System.Windows.Forms.Label();
            this.lblLoginUser = new System.Windows.Forms.Label();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.panelBrand.SuspendLayout();
            this.panelRegister.SuspendLayout();
            this.panelLogin.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelBrand
            // 
            this.panelBrand.BackColor = System.Drawing.Color.Black;
            this.panelBrand.Controls.Add(this.panelRegister);
            this.panelBrand.Controls.Add(this.panelLogin);
            this.panelBrand.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelBrand.Location = new System.Drawing.Point(0, 0);
            this.panelBrand.Name = "panelBrand";
            this.panelBrand.Size = new System.Drawing.Size(1115, 596);
            this.panelBrand.TabIndex = 0;
            // 
            // panelRegister
            // 
            this.panelRegister.BackColor = System.Drawing.Color.White;
            this.panelRegister.Controls.Add(this.btnRegister);
            this.panelRegister.Controls.Add(this.lblRegisterRePassword);
            this.panelRegister.Controls.Add(this.lblRegisterPassword);
            this.panelRegister.Controls.Add(this.lblRegisterFullname);
            this.panelRegister.Controls.Add(this.lblRegisterName);
            this.panelRegister.Controls.Add(this.textBox9);
            this.panelRegister.Controls.Add(this.textBox1);
            this.panelRegister.Controls.Add(this.textBox8);
            this.panelRegister.Controls.Add(this.textBox7);
            this.panelRegister.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panelRegister.Location = new System.Drawing.Point(801, 12);
            this.panelRegister.Name = "panelRegister";
            this.panelRegister.Size = new System.Drawing.Size(302, 560);
            this.panelRegister.TabIndex = 1;
            this.panelRegister.TabStop = false;
            this.panelRegister.Text = "Sign Up!";
            // 
            // btnRegister
            // 
            this.btnRegister.BackColor = System.Drawing.Color.Green;
            this.btnRegister.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRegister.ForeColor = System.Drawing.Color.White;
            this.btnRegister.Location = new System.Drawing.Point(102, 468);
            this.btnRegister.Name = "btnRegister";
            this.btnRegister.Size = new System.Drawing.Size(107, 41);
            this.btnRegister.TabIndex = 7;
            this.btnRegister.Text = "Register";
            this.btnRegister.UseVisualStyleBackColor = false;
            // 
            // lblRegisterRePassword
            // 
            this.lblRegisterRePassword.AutoSize = true;
            this.lblRegisterRePassword.Location = new System.Drawing.Point(6, 365);
            this.lblRegisterRePassword.Name = "lblRegisterRePassword";
            this.lblRegisterRePassword.Size = new System.Drawing.Size(240, 26);
            this.lblRegisterRePassword.TabIndex = 6;
            this.lblRegisterRePassword.Text = "Re-enter your password";
            // 
            // lblRegisterPassword
            // 
            this.lblRegisterPassword.AutoSize = true;
            this.lblRegisterPassword.Location = new System.Drawing.Point(6, 267);
            this.lblRegisterPassword.Name = "lblRegisterPassword";
            this.lblRegisterPassword.Size = new System.Drawing.Size(102, 26);
            this.lblRegisterPassword.TabIndex = 5;
            this.lblRegisterPassword.Text = "Password";
            // 
            // lblRegisterFullname
            // 
            this.lblRegisterFullname.AutoSize = true;
            this.lblRegisterFullname.Location = new System.Drawing.Point(6, 168);
            this.lblRegisterFullname.Name = "lblRegisterFullname";
            this.lblRegisterFullname.Size = new System.Drawing.Size(110, 26);
            this.lblRegisterFullname.TabIndex = 4;
            this.lblRegisterFullname.Text = "Full Name";
            // 
            // lblRegisterName
            // 
            this.lblRegisterName.AutoSize = true;
            this.lblRegisterName.Location = new System.Drawing.Point(6, 72);
            this.lblRegisterName.Name = "lblRegisterName";
            this.lblRegisterName.Size = new System.Drawing.Size(118, 26);
            this.lblRegisterName.TabIndex = 3;
            this.lblRegisterName.Text = "User Name";
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(6, 392);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(290, 30);
            this.textBox9.TabIndex = 2;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(6, 102);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(290, 30);
            this.textBox1.TabIndex = 2;
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(6, 296);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(290, 30);
            this.textBox8.TabIndex = 2;
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(6, 195);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(290, 30);
            this.textBox7.TabIndex = 2;
            // 
            // panelLogin
            // 
            this.panelLogin.BackColor = System.Drawing.Color.White;
            this.panelLogin.Controls.Add(this.btnLogin);
            this.panelLogin.Controls.Add(this.lblLoginPassword);
            this.panelLogin.Controls.Add(this.lblLoginUser);
            this.panelLogin.Controls.Add(this.textBox11);
            this.panelLogin.Controls.Add(this.textBox10);
            this.panelLogin.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panelLogin.Location = new System.Drawing.Point(12, 12);
            this.panelLogin.Name = "panelLogin";
            this.panelLogin.Size = new System.Drawing.Size(302, 560);
            this.panelLogin.TabIndex = 0;
            this.panelLogin.TabStop = false;
            this.panelLogin.Text = "Do you have an account? Login";
            // 
            // btnLogin
            // 
            this.btnLogin.BackColor = System.Drawing.Color.Green;
            this.btnLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogin.ForeColor = System.Drawing.Color.White;
            this.btnLogin.Location = new System.Drawing.Point(101, 468);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(96, 41);
            this.btnLogin.TabIndex = 5;
            this.btnLogin.Text = "Login";
            this.btnLogin.UseVisualStyleBackColor = false;
            // 
            // lblLoginPassword
            // 
            this.lblLoginPassword.AutoSize = true;
            this.lblLoginPassword.Location = new System.Drawing.Point(6, 168);
            this.lblLoginPassword.Name = "lblLoginPassword";
            this.lblLoginPassword.Size = new System.Drawing.Size(102, 26);
            this.lblLoginPassword.TabIndex = 4;
            this.lblLoginPassword.Text = "Password";
            // 
            // lblLoginUser
            // 
            this.lblLoginUser.AutoSize = true;
            this.lblLoginUser.Location = new System.Drawing.Point(6, 72);
            this.lblLoginUser.Name = "lblLoginUser";
            this.lblLoginUser.Size = new System.Drawing.Size(118, 26);
            this.lblLoginUser.TabIndex = 3;
            this.lblLoginUser.Text = "User Name";
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(6, 195);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(290, 30);
            this.textBox11.TabIndex = 2;
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(6, 102);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(290, 30);
            this.textBox10.TabIndex = 2;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1115, 596);
            this.Controls.Add(this.panelBrand);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "                                                                                 " +
    "                               :::-Easy Notes-:::";
            this.panelBrand.ResumeLayout(false);
            this.panelRegister.ResumeLayout(false);
            this.panelRegister.PerformLayout();
            this.panelLogin.ResumeLayout(false);
            this.panelLogin.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelBrand;
        private System.Windows.Forms.GroupBox panelRegister;
        private System.Windows.Forms.Button btnRegister;
        private System.Windows.Forms.Label lblRegisterRePassword;
        private System.Windows.Forms.Label lblRegisterPassword;
        private System.Windows.Forms.Label lblRegisterFullname;
        private System.Windows.Forms.Label lblRegisterName;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.GroupBox panelLogin;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.Label lblLoginPassword;
        private System.Windows.Forms.Label lblLoginUser;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.TextBox textBox10;
    }
}

